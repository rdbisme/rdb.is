CURDIR=$(shell pwd)
PY?=python
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

VERSION=staging


DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

help:
	@echo 'Makefile for a pelican Web site served on GAE                             '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make html                           (re)generate the web site          '
	@echo '   make clean                          remove the generated files         '
	@echo '   make regenerate                     regenerate files upon modification '
	@echo '   make publish                        generate using production settings '
	@echo '   make serve [PORT=8080]              serve site at http://localhost:8080'
	@echo '   make deploy [VERSION=staging]       deploy site to GAE'
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html   '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                    '
	@echo '                                                                          '

copy-gcloud-files: 
	cp $(INPUTDIR)/app.yaml $(OUTPUTDIR)/app.yaml 
	cp $(INPUTDIR)/main.py $(OUTPUTDIR)/main.py

clean-output-dir:
	rm -rf $(OUTPUTDIR)/*

html: clean-output-dir copy-gcloud-files
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

publish: clean-output-dir copy-gcloud-files
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)


clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	dev_appserver.py --port$(PORT) $(OUTPUTDIR)/app.yaml
else
	dev_appserver.py --host=lvh.me  $(OUTPUTDIR)/app.yaml
endif


deploy: publish
	gcloud app deploy -v $(VERSION) --no-promote $(OUTPUTDIR)/app.yaml
	@echo ' '
	@echo 'The deployed version $(VERSION) is not automatically promoted'
	@echo 'to receive all traffic. You need to do it manually'
	@echo ' '
	@echo ' gcloud app services set-traffic default --splits $(VERSION)=1'
	@echo ' '

.PHONY: html help clean regenerate serve deploy
