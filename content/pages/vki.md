Title: von Karman Institute for Fluid Dynamics
Slug: vki

I've been enrolled as a *Research Master Student* at <abbr title="von Karman Institute for Fluid Dynamics">VKI</abbr> with a project that is the continuation of the Master Thesis project I also developed at <abbr title="von Karman Institute for Fluid Dynamics">VKI</abbr> (more informations [here]({filename}/pages/polimi.md)).  In this page you will find all the material developed during this experience and particularly the material related to the exams.

## Industrial Design Exercise
### Numerical study of the spray quenching for a moving metal sheet
![gaussian-spray]({filename}/img/gaussian-spray.png)

Several industrial processes nowadays are often optimized by a trial and error approach, often being a time consuming ant error prone action, potentially leading to sub-optimal solutions. With the substantial increase of the computational capabilities of the modern calculators, linked to the improvement of the quality for the numerical libraries and to the advances in the theoretical approach of Partial Differential Equations (PDEs) solution methods, the introduction of advanced numerical methodologies for the solution of complex models is more and more envisaged. In this work the numerical study of the cooling of a thin metal strip is performed through the use of a Finite Elements Method (FEM) modeling approach assisted by the implementation of empirical correlation for the heat transfer due to impinging sprays. The software stack provided by FEniCS, a LGPL-licensed library for Finite Elements computations, is used. This work was done as part of the Industrial Design Exercise (IDE) exam at von Karman Institute for Fluid Dynamics (VKI)

[Download Full Text](https://drive.google.com/file/d/0B7DR4Jvb-dBReW5selBaWVRQbjg/view?usp=sharing){: .pure-button .pure-button-primary }

## Physico-Chemicals Models for Atmospheric reentry Flows 
### Advanced laboratory on molecular and atomic spectroscopy of plasma flow at Plasmatron facility
![plasmatron]({filename}/img/plasmatron.jpg){: height=400px }

Plasma flows are a very important topic in several applications, nowadays several studies are undergoing to increase the awareness and technical capabilities to predict flow conditions in different industrial and scientific application as for example reentry flow or nuclear fusion. Numerical methods are being developed at von Karman Institute for Fluid Dynamics (VKI), but to achieve a reliable and complete knowledge of the topic they need to be coupled with reliable experimental campaigns. In this report the experimental characterization of the plasma flow obtained in the Plasmatron facility of VKI is discussed, first analyzing the free stream plasma emission, than taking into account the graphite ablation of a bluff body due to hot plasma stream.
This report is a side-work due for the Physico-Chemical Models for Atmospheric Entry Flows (PCMAF) course of Prof. Magin. The experimental activity has been supervised by Dr. Bernd Helber.


[Download Full Text](https://drive.google.com/open?id=0B7DR4Jvb-dBRbHhmRzRxa19wMjg){: .pure-button .pure-button-primary }

## Numerical Simulation of Industrial Flows II
### CFD modeling and validation of the flow inside axisymmetric expansions
![axial-streamlines]({filename}/img/axial-streamlines.png){: width=90% }

The increase of computational power nowadays make CFD a technology more and more interesting to simulate an always wider class of industrial problems. The vast majority of these industrial flows are characterized by turbulent flows. The Reynolds-averaged Navier- Stokes (RANS) approach is de-facto standard today for the numerical modeling of turbulent industrial flows and while more advanced approach like LES are still too much computational expensive to be employed successfully on averagely complex configurations, cheap global models are in high demand for lots of different applications. Turbulence models are supposed to fill the gap of several orders of magnitude in computational effort with just several tunable parameters that are often optimized for simply well-known cases. In this case the different flavours of k − ε and the and k − ω SST turbulence models, are validated against experimental data of flow inside axisymmetric expansions within the CFD framework provided by the Free and Open Source Software (FOSS) Open Field Operation and Manipulation (OpenFOAM). The different turbulence models are benchmarked in a low-y+ scenario with study performed on the effect of mesh refinement

[Download Full Text](https://drive.google.com/file/d/0B7DR4Jvb-dBRTmFJdk5jZ0w2Ym8/view?usp=sharing){: .pure-button .pure-button-primary}

## Numerical Simulation of Industrial Flows
### CFD modeling and validation of a turbulent flow over a flat plate
![turbulent-viscosity]({filename}/img/turbulent-viscosity.png){: width=90%}

The increase of computational power nowadays make CFD a technology more and more interesting to simulate an always wider class of industrial problems. The vast majority of these industrial flows are characterized by turbulent flows. The Reynolds-averaged Navier-Stokes (RANS) approach is de-facto standard today for the numerical modeling of turbulent industrial flows and while more advanced approach like LES are still too much computational expensive to be employed successfully on averagely complex configurations, cheap global models are in high demand for lots of different applications. Turbulence models are supposed to fill the gap of several orders of magnitude in computational effort with just several tunable parameters that are often optimized for simply well-known cases. In this case the three most common turbulence models, Spalart-Allmaras, k − ε and k − ω SST, are validated against experimental data over a flat plate within the CFD framework provided by the Numeca FINE/Open software. A study on the mesh dependency and the numerical schemes influence is provided together with a quality assessment of the CFD simulation on the base of the provided experimental data.


[Download Full Text](https://drive.google.com/file/d/0B7DR4Jvb-dBRZ1hPOTBUWUF2c2M/view?usp=sharing){: .pure-button .pure-button-primary }


## Measurement Techniques Labs
### Aerodynamic loads measurement and flow visualization of an ogive in a supersonic flow
![schlieren]({filename}/img/schlieren.jpg){height=300px}

The behavior of supersonic flow around a slender body (a von Karman-shaped nose-cone) was studied in this work. The nose-cone, or ogive, was subjected to a flow at Mach 3.5 in the S-4 blow-down wind tunnel. The forces on the ogive were measured for different angles of attack by use of an internal 3-component sting balance. The calibration method and the data acquisition chain are presented and the results are commented accordingly. The resulting force and moment coefficients of the ogive were thereafter compared to analytical solutions. Good accordance is reported for both axial and lift coefficients, while the drag coefficient is underpredicted by the theoretical methods. The shockwave behavior around the ogive is studied by three visualisation techniques: shadowgraph and schlieren, oil visualisation and by use of a water-table. Both the shadowgraph and oil visualisation experiments were performed in the S-4 wind tunnel. The results were compared to theoretical shock behavior and to the hydraulic analogy provided by the water-table experiments. Merging the outcomes of the schlieren, oil visualization and water table was possible to propose an interpretation of the flow pattern while the shadowgraph was unable to provide meaningful results.

[Download Full Text](https://drive.google.com/file/d/0B7DR4Jvb-dBRY2FuSUU5S0huWWs/view?usp=sharing){: .pure-button .pure-button-primary }
